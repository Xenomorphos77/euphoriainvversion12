import { initializeApp } from 'firebase/app'
import { getAuth } from 'firebase/auth'
import { getDatabase } from 'firebase/database';

const firebaseConfig = {
    apiKey: "AIzaSyCJJzVO1Bxn-cD6vWrUaYWb6vBp79wU950",
    authDomain: "euphoriainv-49d22.firebaseapp.com",
    projectId: "euphoriainv-49d22",
    storawageBucket: "euphoriainv-49d22.appspot.com",
    messagingSenderId: "215762636324",
    appId: "1:215762636324:web:4a43aa2b5c541aa497d2b1",
    measurementId: "G-9M2LSR6MLQ"
};

const app = initializeApp(firebaseConfig)
const auth = getAuth(app)
const items = getDatabase(app)

export default {auth, items}

